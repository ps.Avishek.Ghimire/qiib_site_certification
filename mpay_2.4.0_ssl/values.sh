#!/bin/bash

export filename="${1}"

cat >"$filename" <<EOF
global:
  psci_version:           "${PSCI_VERSION}"
  psci_devops_domain:     "${PSCI_DEVOPS_DOMAIN}"
  psci_registry_url:      "${PSCI_REGISTRY_URL}"
  psci_registry_username: "${PSCI_REGISTRY_USERNAME}"
  psci_registry_password: "${PSCI_REGISTRY_PASSWORD}"
  spring_url:             "https://${CI_GROUP_NAME}-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}-qiib-apis.${PSCI_DEVOPS_DOMAIN}/psp/message"

qiib-ui:
  image:
    imageName: "${CI_GROUP_NAME}/${CI_PROJECT_NAME}-qiib-ui"

qiib-apis:
  image:
    imageName: "${CI_GROUP_NAME}/${CI_PROJECT_NAME}-qiib-apis"

EOF
