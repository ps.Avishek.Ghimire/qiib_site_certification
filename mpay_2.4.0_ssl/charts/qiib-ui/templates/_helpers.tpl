{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "qiib-ui.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "qiib-ui.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "qiib-ui.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "qiib-ui.labels" -}}
app.kubernetes.io/name: {{ include "qiib-ui.name" . }}
helm.sh/chart: {{ include "qiib-ui.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Default version
*/}}
{{- define "qiib-ui.version" -}}
{{- if .Values.image.tag -}}
{{- .Values.image.tag -}}
{{- else -}}
{{- if .Values.global -}}
{{- .Values.global.psci_version | default "latest" }}
{{- else -}}
{{- "latest" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Default Repo
*/}}
{{- define "qiib-ui.repo" -}}
{{- if .Values.image.repo -}}
{{- .Values.image.repo -}}
{{- else -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_url | default "harbor.progressoft.io" }}
{{- else -}}
{{- "harbor.progressoft.io" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Default Database Engine Params
*/}}
{{- define "databaseEngine" -}}
{{- if .Values.global -}}
{{- .Values.global.database_engine   | default "h2" -}}
{{- else -}}
{{- "h2" -}}
{{- end -}}
{{- end -}}

{{- define "databaseUsername" -}}
{{- if .Values.global -}}
{{- .Values.global.database_username | default (printf "%s" .Release.Name | replace "-" "" | trunc 20) -}}
{{- else -}}
{{- printf "%s" .Release.Name | replace "-" "" | trunc 20 -}}
{{- end -}}
{{- end -}}

{{- define "databasePassword" -}}
{{- if .Values.global -}}
{{- .Values.global.database_username | default (printf "%s" .Release.Name | replace "-" "" | trunc 20) -}}
{{- else -}}
{{- printf "%s" .Release.Name | replace "-" "" | trunc 20 -}}
{{- end -}}
{{- end -}}

{{/*
Domain
*/}}
{{- define "devopsDomain" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_devops_domain | default "progressoft.dev" }}
{{- else -}}
{{- "progressoft.dev" -}}
{{- end -}}
{{- end -}}

{{/*
Registry
*/}}
{{- define "registryHost" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_url | default "localhost:32000" }}
{{- else -}}
{{- "localhost:32000" -}}
{{- end -}}
{{- end -}}

{{- define "registryUser" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_username | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}

{{- define "registryPass" -}}
{{- if .Values.global -}}
{{- .Values.global.psci_registry_password | default "" }}
{{- else -}}
{{- "" -}}
{{- end -}}
{{- end -}}
