**1) Generate the certificate to login to kubeadmin**

`[root@iihobastion01 mpay]# oc rsh -n openshift-authentication oauth-openshift-8d547cf49-gl87z cat /run/secrets/kubernetes.io/serviceaccount/ca.crt > ~/.kube/ingress-ca.crt`

**2) Login to kubeadmin**

`[root@iihobastion01 mpay]# oc login -u kubeadmin -p L8eLU-wKFxB-vBhLx-eLod2 --certificate-authority=/root/.kube/ingress-ca.crt`

**3) Login to image registry**

`[root@iihobastion01 mpay]# podman login -u kubeadmin -p $(oc whoami -t) default-route-openshift-image-registry.apps.ocp-prod.qiibonline.com --tls-verify=false`

**4) Load the image tar to local repository**

`[root@iihobastion01 mpay]# podman load < image.tar`

**5) Tag the image using registry of ocp**

`[root@iihobastion01 mpay]# podman tag <source image name> default-route-openshift-image-registry.apps.ocp-prod.qiibonline.com/mpay/activemq-artemis1:2.8-alpine-latest`

**6) Push the image to the ocp registry**

`[root@iihobastion01 mpay]# podman push default-route-openshift-image-registry.apps.ocp-prod.qiibonline.com/mpay/activemq-artemis1:2.8-alpine-latest --tls-verify=false`
